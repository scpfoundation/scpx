class RenameTypeColumnInMessages < ActiveRecord::Migration
  def change
    remove_column :messages, :type, :string
    add_column :messages, :message_type, :string
  end
end
