class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :name
      t.text :description

      t.timestamps
    end

    create_table :pages_tags, id: false do |t|
      t.integer :page_id, null: false
      t.integer :tag_id, null: false
    end
    
    add_index :tags, :name, unique: true
  end
end
