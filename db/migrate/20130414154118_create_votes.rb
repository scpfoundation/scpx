class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.integer :user_id, null: false
      t.integer :page_id, null: false
      t.integer :value, default: 0, null: false
      t.decimal :result, default: 0.0, null: false
      t.boolean :admin, default: false, null: false
      t.boolean :staff, default: false, null: false

      t.timestamps
    end

    add_column :pages, :translation, :boolean, default: false, null: false
    add_column :pages, :rating, :decimal, default: 0.0, null: false
    add_column :pages, :admin_rating, :integer, default: 0, null: false
    add_column :pages, :staff_rating, :integer, default: 0, null: false
    add_column :users, :power, :decimal, default: 1.0, null: false
  end
end
