class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :nickname
      t.string :authname
      t.text :content
      t.string :type
      t.string :channel

      t.timestamps
    end
  end
end
