class CreateAccessTokens < ActiveRecord::Migration
  def change
    create_table :access_tokens do |t|
      t.string :name
      t.string :token
      t.string :access

      t.timestamps
    end

    add_index :access_tokens, :name, unique: true
  end
end
