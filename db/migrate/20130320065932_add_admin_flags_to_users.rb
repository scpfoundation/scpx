class AddAdminFlagsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :admin, :boolean
    add_column :users, :staffer, :boolean
    add_column :users, :operator, :boolean
  end
end
