class CreateWarnings < ActiveRecord::Migration
  def change
    create_table :warnings do |t|
      t.integer :user_id
      t.text :description
      t.integer :staffer_id

      t.timestamps
    end
  end
end
