class CreateVersions < ActiveRecord::Migration
  def change
    create_table :versions do |t|
      t.integer :user_id
      t.text :content
      t.string :title
      t.string :commit_message
      t.references :versionable, polymorphic: true

      t.timestamps
    end
  end
end
