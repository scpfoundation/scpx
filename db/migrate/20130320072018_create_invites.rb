class CreateInvites < ActiveRecord::Migration
  def change
    create_table :invites do |t|
      t.string :email
      t.integer :user_id
      t.string :token

      t.timestamps
    end
  end
end
