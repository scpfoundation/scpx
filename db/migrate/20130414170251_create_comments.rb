class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :user_id, null: false
      t.integer :page_id, null: false
      t.text :content, null: false
      t.string :ancestry, default: ''

      t.timestamps
    end

    add_index :comments, :ancestry
    add_index :comments, :user_id
    add_index :comments, :page_id
  end
end
