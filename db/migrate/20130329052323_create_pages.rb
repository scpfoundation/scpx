class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.text :content, default: '', null: false
      t.string :name, default: '', null: false
      t.string :slug, null: false
      t.string :commit_message, default: '', null: false
      t.integer :namespace_id, null: false
      t.integer :user_id, null: false

      t.timestamps
    end

    add_index :pages, :slug, unique: true

    create_table :pages_users, id: false do |t|
      t.integer :page_id, null: false
      t.integer :user_id, null: false
    end

    add_index :pages_users, :page_id
    add_index :pages_users, :user_id
  end
end
