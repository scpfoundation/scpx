# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130713140503) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "access_tokens", force: true do |t|
    t.string   "name"
    t.string   "token"
    t.string   "access"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "access_tokens", ["name"], name: "index_access_tokens_on_name", unique: true, using: :btree

  create_table "comments", force: true do |t|
    t.integer  "user_id",                 null: false
    t.integer  "page_id",                 null: false
    t.text     "content",                 null: false
    t.string   "ancestry",   default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["ancestry"], name: "index_comments_on_ancestry", using: :btree
  add_index "comments", ["page_id"], name: "index_comments_on_page_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "invites", force: true do |t|
    t.string   "email"
    t.integer  "user_id"
    t.string   "token"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: true do |t|
    t.string   "nickname"
    t.string   "authname"
    t.text     "content"
    t.string   "channel"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "message_type"
  end

  create_table "namespaces", force: true do |t|
    t.string   "name",                                  null: false
    t.string   "title",      default: "SCP Foundation", null: false
    t.string   "access",     default: "members",        null: false
    t.boolean  "discussion", default: false,            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "namespaces", ["name"], name: "index_namespaces_on_name", unique: true, using: :btree

  create_table "notifications", force: true do |t|
    t.integer  "user_id",                    null: false
    t.string   "content",                    null: false
    t.boolean  "read",       default: false, null: false
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pages", force: true do |t|
    t.string   "title"
    t.text     "content",        default: "",    null: false
    t.string   "name",           default: "",    null: false
    t.string   "slug",                           null: false
    t.string   "commit_message", default: "",    null: false
    t.integer  "namespace_id",                   null: false
    t.integer  "user_id",                        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "editor_id"
    t.boolean  "translation",    default: false, null: false
    t.decimal  "rating",         default: 0.0,   null: false
    t.integer  "admin_rating",   default: 0,     null: false
    t.integer  "staff_rating",   default: 0,     null: false
  end

  add_index "pages", ["slug"], name: "index_pages_on_slug", unique: true, using: :btree

  create_table "pages_tags", id: false, force: true do |t|
    t.integer "page_id", null: false
    t.integer "tag_id",  null: false
  end

  create_table "pages_users", id: false, force: true do |t|
    t.integer "page_id", null: false
    t.integer "user_id", null: false
  end

  add_index "pages_users", ["page_id"], name: "index_pages_users_on_page_id", using: :btree
  add_index "pages_users", ["user_id"], name: "index_pages_users_on_user_id", using: :btree

  create_table "tags", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "email"
    t.string   "display_name"
    t.string   "password_digest"
    t.string   "real_name"
    t.text     "about"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.string   "auth_token"
    t.boolean  "admin"
    t.boolean  "staffer"
    t.boolean  "operator"
    t.decimal  "power",                  default: 1.0, null: false
  end

  add_index "users", ["auth_token"], name: "index_users_on_auth_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "versions", force: true do |t|
    t.integer  "user_id"
    t.text     "content"
    t.string   "title"
    t.string   "commit_message"
    t.integer  "versionable_id"
    t.string   "versionable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "votes", force: true do |t|
    t.integer  "user_id",                    null: false
    t.integer  "page_id",                    null: false
    t.integer  "value",      default: 0,     null: false
    t.decimal  "result",     default: 0.0,   null: false
    t.boolean  "admin",      default: false, null: false
    t.boolean  "staff",      default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "warnings", force: true do |t|
    t.integer  "user_id"
    t.text     "description"
    t.integer  "staffer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
