
# Default system user
system_user = {
  username: 'Hatbot',
  password: '682mustbefree',
  password_confirmation: '682mustbefree',
  email: 'robot@scpfoundation.net',
  real_name: 'Administrative Robot',
  staffer: true,
  admin: true
}

User.create!(system_user) if User.count == 0

# Default set of the namespaces
namespaces = [
  {
    name: 'main',
    access: 'everyone'
  },

  {
    name: 'staff',
    access: 'staffers'
  },

  {
    name: 'talks',
    title: 'Talks',
    discussion: true
  },

  {
    name: 'news',
    title: 'News',
  },

  {
    name: 'o5',
    title: 'O5',
    access: 'admins'
  },

  {
    name: 'meta',
    title: 'Meta',
  },

  {
    name: 'sandbox',
    title: 'Sandbox',
  },

  {
    name: 'inbox',
    access: 'private'
  },

  {
    name: 'drafts',
    title: 'Drafts',
    access: 'private'
  },

  {
    name: 'archive',
    title: 'Archive',
  }
]

Namespace.create! namespaces if Namespace.all.count == 0

