require 'spec_helper'

describe "PasswordResets" do
  it "emails user when requesting password reset" do
    user = FactoryGirl.create :user
    visit login_path
    click_link I18n.t('sessions.new.forgotten_password')
    fill_in "user_username", with: user.username
    click_button I18n.t('password_resets.new.password_reset')
    page.should have_content(I18n.t('password_resets.email_send_notice'))
    last_email.to.should include(user.email)
  end

  it "does not email invalid user when requesting password reset" do
    visit login_path
    click_link I18n.t('sessions.new.forgotten_password')
    fill_in "user_username", with: "SuperUser"
    click_button I18n.t('password_resets.new.password_reset')
    page.should have_content(I18n.t('password_resets.no_user_warning'))
    last_email.should be_nil
  end
end
