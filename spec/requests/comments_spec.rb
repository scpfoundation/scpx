require 'spec_helper'
describe 'Comments', js: true do
  let(:user) { create(:user, staffer: true) }
  let(:namespace) { Namespace.find_by_name('main') }
  let(:test_page) { create(:page, namespace: namespace, user: user) }

  it 'should create comment' do
    login_as user

    visit page_comments_path(test_page, subdomain: namespace.name)
    click_on I18n.t('comments.index.reply_to_this_topic')
    fill_in 'wmd-input', with: 'Test comment!'
    click_button I18n.t('comments.form.reply')
    page.should have_content 'Reply was added'
    page.should have_content 'Test comment!'
    click_on I18n.t('comments.form.reply')
    fill_in 'wmd-input', with: 'Test reply!'
    click_button I18n.t('comments.form.reply')
    page.should have_content 'Reply was added'
    page.should have_content 'Test comment!'
    page.should have_content 'Test reply!'
  end

  it 'should create notification' do
    login_as user

    another_user = create :user
    visit page_comments_path(test_page, subdomain: namespace.name)
    click_on I18n.t('comments.index.reply_to_this_topic')
    fill_in 'wmd-input', with: "@#{another_user.username} mention."
    click_button I18n.t('comments.form.reply')
    page.should have_content 'Reply was added'
    page.should have_content "@#{another_user.username} mention."
    Notification.last.user.should eq another_user
  end
end
