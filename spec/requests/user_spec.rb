require 'spec_helper'

describe "User" do
  describe "#registration" do
    let(:attr) { attributes_for :user }
    let(:invite) { create :invite, email: attr[:email] }

    before(:each) do
      visit registration_path
      fill_in "user_username", with: attr[:username]
      fill_in "user_real_name", with: attr[:real_name]
      fill_in "user_password", with: attr[:password]
      fill_in "user_password_confirmation", with: attr[:password_confirmation]
    end

    it "should register user with correct attributes" do
      fill_in "user_invite_code", with: invite.token
      click_button I18n.t('users.new.sign_up')

      expect(page).to have_content I18n.t('users.create.signed_up')
      expect(page).to have_content attr[:username]
      expect(User.last.email).to eq attr[:email]
    end
  end
end
