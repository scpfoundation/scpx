FactoryGirl.define do
  factory :user do
    sequence(:username) { |n| "TestUser#{n}" }
    sequence(:real_name) { |n| "Test User #{n}" }
    sequence(:email) { |n| "test#{n}@example.com" }
    sequence(:about) { |n| "About Text ##{n}" }
    password 'foobar'
    password_confirmation 'foobar'
  end

  factory :invite do
    sequence(:email) { |n| "test#{n}@example.com" }
    user_id { create(:user) }
  end

  factory :namespace do
    sequence(:name) { |n| "test-namespace-#{n}" }
    sequence(:title) { |n| "Test Namespace ##{n}" }
  end

  factory :page do
    sequence(:title) { |n| "Page #{n}" }
    user { create(:user) }
    sequence(:content) { |n| "Lorem Ipsum #{n}" }
    namespace { create(:namespace) }
    sequence(:commit_message) { |n| "new page #{n}, yea" }
    sequence(:name) { |n| "test-page-#{n}" }
  end

  factory :tag do
    sequence(:name) { |n| "test-tag-#{n}" }
    sequence(:description) { |n| "Test Tag Descripton ##{n}" }
  end

  factory :vote do
    user { create(:user) }
    page { create(:page) }
    value { 1 }
  end

  factory :comment do
    user { create(:user) }
    page { create(:page) }
    sequence(:content) { |n| "test comment #{n}" }
    parent_id { nil }
  end

  factory :notification do
    sequence(:content) { |n| "Sample notification #{n}" }
    user { create(:user) }
    url { |n| "http://example.com/#{n}" }
  end
end
