module AuthMacros
  def logout_user(user = @current_user)
      Capybara.reset_sessions!
      visit root_url
      click_link :logout
  end

  def login_as(user)
    visit new_session_path

    fill_in "session_username", with: user.username
    fill_in "session_password", with: user.password

    click_button I18n.t('sessions.new.sign_in')

    page.should have_css("div.alert-success")
  end
end
