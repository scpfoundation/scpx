require 'spec_helper'

describe Ability do
  describe 'as guest' do
    let(:ability) { Ability.new nil }
    subject { ability }

    it { should be_able_to :create, User }
    it { should be_able_to :create, :session }
    it { should be_able_to :create, :password_reset }
    it { should be_able_to :read, FactoryGirl.create(:namespace, access: 'everyone') }
    it { should be_able_to :read, Tag }
  end

  describe 'as user' do
    let(:user) { FactoryGirl.create :user }
    let(:ability) { Ability.new user }
    let(:vote) { create(:vote, user: user) }
    let(:comment) { create(:comment, user: user) }
    let(:notification) { create(:notification, user: user) }
    subject { ability }

    it { should be_able_to :read, User }
    it { should be_able_to :destroy, :session }
    it { should_not be_able_to :create, User }
    it { should_not be_able_to :create, :session }
    it { should_not be_able_to :create, :password_reset }
    it { should be_able_to :read, FactoryGirl.create(:namespace, access: 'private') }
    it { should be_able_to :use, FactoryGirl.create(:namespace, access: 'private') }
    it { should be_able_to :read, FactoryGirl.create(:namespace, access: 'members') }
    it { should be_able_to :use, FactoryGirl.create(:namespace, access: 'members') }
    it { should be_able_to :read, Vote }
    it { should be_able_to :create, Vote }
    it { should be_able_to :update, vote }
    it { should be_able_to :destroy, vote }
    it { should be_able_to :vote, Page }
    it { should be_able_to :read, Comment }
    it { should be_able_to :create, Comment }
    it { should be_able_to :update, comment }
    it { should be_able_to :index, Notification }
    it { should be_able_to :read_all, Notification }
    it { should be_able_to :destroy_read, Notification }
    it { should be_able_to :show, notification }
    it { should be_able_to :destroy, notification }
    it { should be_able_to :edit, user }
    it { should be_able_to :update, user }
    it { should be_able_to :read, Message }
  end

  describe 'as staffer' do
    let(:user) { FactoryGirl.create(:user, staffer: true) }
    let(:ability) { Ability.new user }
    subject { ability }
    it { should be_able_to :read, FactoryGirl.create(:namespace, access: 'staffers') }
    it { should be_able_to :use, FactoryGirl.create(:namespace, access: 'staffers') }
    it { should be_able_to :use, FactoryGirl.create(:namespace, access: 'everyone') }
    it { should be_able_to :manage, Warning }
    it { should be_able_to :use, :admin_namespace }
  end

  describe 'as admin' do
    let(:user) { FactoryGirl.create(:user, staffer: true, admin: true) }
    let(:ability) { Ability.new user }
    subject { ability }

    it { should be_able_to :read, Invite }
    it { should be_able_to :destroy, Invite }
    it { should be_able_to :create, Invite }
    it { should be_able_to :read, FactoryGirl.create(:namespace, access: 'o5') }
    it { should be_able_to :use, FactoryGirl.create(:namespace, access: 'o5') }
    it { should be_able_to :manage, Tag }
    it { should be_able_to :destroy, Vote }
    it { should be_able_to :destroy, Comment }
    it { should be_able_to :edit, User }
    it { should be_able_to :update, User }
  end
end
