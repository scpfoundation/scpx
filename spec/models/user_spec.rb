require 'spec_helper'

describe User do
  let(:attr) do
    {
      username: 'SampleUser',
      email: 'sample@user.com',
      password: 'foobar',
      password_confirmation: 'foobar',
      real_name: 'Sample User'
    }
  end

  it "should correctly save display_name" do
    user = User.create attr
    expect(user.username).to eq 'sampleuser'
    expect(user.display_name).to eq 'SampleUser'
  end

  it "should return username when calling to_param" do
    user = FactoryGirl.create :user
    expect(user.to_param).to eq user.username
  end

  it "should generate auth token" do
    user = FactoryGirl.create :user
    expect(user.auth_token).not_to be_nil
    expect(user.auth_token).not_to be_empty
  end

  describe "#send_password_reset" do
    let(:user) { FactoryGirl.create :user }

    it "generates a uniqie password_reset_token each time" do
      user.send_password_reset
      last_token = user.password_reset_token
      user.send_password_reset
      user.password_reset_token.should_not eq(last_token)
    end

    it "saves the time the password reset was sent" do
      user.send_password_reset
      user.reload.password_reset_sent_at.should be_present
    end

    it "delivers email to user" do
      user.send_password_reset
      last_email.to.should include (user.email)
    end
  end
end
