require 'spec_helper'

describe Invite do
  subject { FactoryGirl.create(:invite) }

  its(:token) { should_not be_nil }
  
  it "should not create if there is already user with same email" do
    user = create :user
    invite_attr = attributes_for(:invite)
    invite_attr[:email] = user.email
    invite = Invite.new invite_attr
    invite.save.should_not be_true
    invite.errors[:email].first.should eq 'User or invite with the same email exists'
  end
end
