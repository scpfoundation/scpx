require 'spec_helper'

describe Namespace do
  it "should return name from to_param" do
    namespace = FactoryGirl.create :namespace, name: 'supernamespace'
    expect(namespace.to_param).to eq 'supernamespace'
  end
end
