require 'spec_helper'

describe Page do
  let(:namespace) { create(:namespace) }
  let(:page) { create(:page, namespace: namespace) }
  let(:subject) { page }

  its(:slug) { should eq "#{namespace.name}:#{page.name}" }
  its(:to_param) { should eq "#{page.name}" }

  describe "#versions" do
    it "should create versions" do
      text_1 = page.content
      page.update_attributes content: "another text"
      page.versions.count.should eq 1
      page.versions.last.content.should eq text_1
      page.update_attributes content: "lorem rocks"
      page.versions.count.should eq 2
      page.versions.last.content.should eq "another text"
    end

    it "should rollback correctly" do
      text_1 = page.content
      page.content = "another text"
      page.save!
      page.rollback(page.versions.last.id)
      page.content.should eq text_1
    end

    it "should create access list correctly" do
      author = create :user
      users = [create(:user), create(:user), create(:user)]
      page = create :page, user: author
      page.access_list = "#{users.map { |u| u.username }.join(',')}"
      page.save
      expect(page.users.to_a).to eq users.unshift(author)
    end

    it "should create tag list correctly" do
      tags = [create(:tag), create(:tag), create(:tag)]
      page = create :page
      page.tag_list = "#{tags.map { |u| u.name }.join(',')}"
      page.save
      expect(page.tags.to_a).to eq tags
    end
  end
end
