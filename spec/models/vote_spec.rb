require 'spec_helper'

describe Vote do
  it "should count votes correctly" do
    page = create(:page)
    users = [create(:user), create(:user), create(:user), create(:user, staffer: true), create(:user, staffer: true), create(:user, admin: true), create(:user)]
    v = 1
    users.each do |u|
      page.votes.create(user: u, value: v, admin: (u.admin ? true : false) , staff: (u.staffer ? true : false))
      v *= -1
    end

    page = Page.find(page.id)

    expect(page.rating.to_f).to eq 1.0
    expect(page.admin_rating.to_f).to eq -1.0
    expect(page.staff_rating.to_f).to eq 0.0
  end
end
