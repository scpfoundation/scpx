# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :warning do
    user_id 1
    description "MyText"
    staffer_id 1
  end
end
