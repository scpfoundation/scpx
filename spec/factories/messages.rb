# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :message do
    nickname "MyString"
    authname "MyString"
    content "MyText"
    type ""
    channel "MyString"
  end
end
