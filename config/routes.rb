SCPX::Application.routes.draw do
  get "messages/index"
  get "messages/show"
  resources :tags
  resources :namespaces
  resources :invites
  resources :users do
    resources :warnings
  end
  resources :sessions
  resources :password_resets
  namespace :admin do
    get :warnings
  end

  resources :notifications, only: [:index, :show, :destroy]
  post '/notifications/read_all' => 'notifications#read_all', as: :read_all_notifications
  post '/notifications/destroy_read' => 'notifications#destroy_read', as: :destroy_read_notifications

  get '/edits' => 'main#edits', as: :recent_edits
  get '/new_pages' => 'main#new_pages', as: :new_pages
  get '/comments' => 'main#comments', as: :recent_comments
  get '/inbox' => 'main#inbox', as: :inbox

  get 'registration', to: 'users#new', as: 'registration'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'


  constraints subdomain: /^[A-Za-z0-9-]+$/ do
    get '/' => 'namespaces#show'
    get '/pages' => 'pages#index'
    post '/pages' => 'pages#create'
    resources :pages, path: ''
    resources :pages, path: '' do
      resources :versions, only: [:index, :show] do
        get 'rollback', on: :member
        get 'diff', on: :collection
      end
      resources :comments
      resources :votes, only: [:index, :create, :update, :destroy]
    end
  end

  get '/' => 'namespaces#show'
  get '/pages' => 'pages#index'
  match 'api/markdown', to: 'main#markdown_preview', via: [:get, :post], as: :markdown_preview
  resources :messages
  post '/pages' => 'pages#create'
  resources :pages, path: '' do
    resources :versions, only: [:index, :show] do
      get 'rollback', on: :member
      get 'diff', on: :collection
    end
    resources :comments
    resources :votes, only: [:index, :create, :update, :destroy]
  end

  root to: 'namespaces#show'
end
