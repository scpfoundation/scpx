module CommentsHelper

  def comment_note comment
    timestamp_part = l( comment.created_at, format: :timestamp )
    timestamp_part = "#{t('comments.helper.comment_note.redacted')} #{l(comment.updated_at, format: :timestamp )}" if comment.updated_at != comment.created_at

    "#{t('comments.helper.comment_note.wrote')} #{comment.user.display_name}, #{timestamp_part}."
  end

end
