class Version < ActiveRecord::Base
  def user
    @user ||= User.find user_id
  end

  def originator
    user.username
  end
end
