class AccessToken < ActiveRecord::Base
  before_validation :prepare_values, :generate_token

  validates :name, uniqueness: { case_sensitive: false }, format: /\A[\w\_\-\d]+\Z/,  length: { within: 2..30 }

  def to_param
    name
  end

  def generate_token
    begin
      self['token'] = SecureRandom.urlsafe_base64
    end while AccessToken.exists?(token: self['token'])
  end

  private

  def prepare_values
    write_attribute :name, read_attribute(:name).downcase
  end
end
