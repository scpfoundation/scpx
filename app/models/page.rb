class Page < ActiveRecord::Base
  belongs_to :namespace
  belongs_to :user
  has_and_belongs_to_many :users
  has_and_belongs_to_many :tags
  has_many :votes
  has_many :comments

  attr_accessor :access_list, :tag_list

  before_validation :prepare_values
  after_validation :update_slug
  before_update :make_version
  before_save :prepare_access_list, :prepare_tag_list

  validates :name, format: /\A[\w\_\d\-]+\Z/, if: :check_name?
  validate :check_name_uniqueness
  validates :title, presence: true, length: { minimum: 2 }
  validates_associated :namespace

  has_many :versions, as: :versionable

  def to_param
    name
  end

  def editor
    @editor ||= editor_id.nil? ? user : User.find(editor_id)
  end

  def originator
    editor.username
  end

  def rollback version_id
    version = versions.find version_id
    update_attributes(
      title: version.title,
      content: version.content,
      commit_message: "Rollback to revision ##{version_id}")
  end

  private

  def prepare_values
    if read_attribute(:name).nil? || read_attribute(:name).blank?
      write_attribute :name, read_attribute(:title).parameterize
    else
      write_attribute :name, read_attribute(:name).parameterize
    end
  end

  def update_slug
    write_attribute :slug, (namespace.name.to_s + ':' + read_attribute(:name).to_s)
  end

  def check_name?
    name_changed? && !read_attribute(:name).blank?
  end

  def check_name_uniqueness
    page = Page.find_by_slug "#{namespace.name}:#{read_attribute(:name)}"
    if !page.nil? && (self.id.nil? || page.id != self.id)
      errors.add(:name, 'name is not uniq')
    end
  end

  def make_version
    page = Page.find id
    versions.create!(
      title: page.title,
      content: page.content,
      user_id: page.editor.id,
      commit_message: page.commit_message)
  end

  def prepare_access_list
    self.access_list ||= ''
    access = self.access_list.gsub(' ', '').downcase
    list = []

    access.split(',').each do |u|
      next unless u =~ /\A[\w\_\d\-]+\Z/
      permitted_user = User.find_by_username u
      list << permitted_user if permitted_user
    end

    self.users = list.unshift(self.user)
  end

  def prepare_tag_list
    self.tag_list ||= ''
    new_tag_list = self.tag_list.gsub(' ', '').downcase

    list = []

    new_tag_list.split(',').each do |t|
      next unless t =~ /\A[\wа-яА-Я\d\-\_]*\Z/
      tag = Tag.find_by_name t
      list << tag if tag
    end

    self.tags = list
  end
end
