class Tag < ActiveRecord::Base
  has_and_belongs_to_many :pages

  validates :name, format: /\A[\wа-яА-Я\d\-\_]*\Z/, uniqueness: { case_sensitive: false }
  before_save { write_attribute(:name, read_attribute(:name).downcase) }

  def to_param
    name
  end
end
