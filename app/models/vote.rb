class Vote < ActiveRecord::Base
  validates :value, inclusion: { in: [-1, 0, 1] }

  belongs_to :page
  belongs_to :user

  before_save :count_result, :check_user_status
  after_save :count_rating
  after_destroy :count_rating

  private

  def count_result
    write_attribute :result, (read_attribute(:value) * user.power)
  end

  def check_user_status
    if read_attribute(:staff) == true
      write_attribute(:staff, false) unless user.staffer?
      if user.admin?
        write_attribute(:staff, false)
        write_attribute(:admin, true)
      end
    end
  end

  def count_rating
    page.update_column :rating, page.votes.map { |vote| vote.result }.sum
    page.update_column :staff_rating, page.votes.where(staff: true).map { |vote| vote.value }.sum
    page.update_column :admin_rating, page.votes.where(admin: true).map { |vote| vote.value }.sum
  end
end
