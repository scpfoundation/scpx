class Namespace < ActiveRecord::Base
  has_many :pages
  after_create :create_index_page

  def to_param
    name
  end

  def index_page
    @index_page ||= Page.find_by_slug("#{name}:index")
  end

  private

  def create_index_page
    pages.create!(
      user_id: User.find_by_username('hatbot').id,
      title: read_attribute(:name).capitalize,
      content: 'Default index page',
      name: 'index')
  end
end
