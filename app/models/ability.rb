class Ability
  include CanCan::Ability

  def initialize(user)
    can :create, User
    can :create, :session
    can :create, :password_reset
    can :read, Namespace, access: 'everyone'
    can :read, Page, ["namespace_id IN (SELECT id FROM namespaces WHERE access = 'everyone')"] do |page|
      page.namespace.access == 'everyone'
    end
    can :read, Tag

    if user
      can [:read, :use], Namespace, access: 'private'
      can [:read, :use], Namespace, access: 'members'
      can [:read, :create], Comment
      can :update, Comment, user_id: user.id
      can :read, Message

      can [:index, :read_all, :destroy_read], Notification
      can [:show, :destroy], Notification, user_id: user.id

      can [:read, :update, :comment], Page, ["id IN (SELECT page_id FROM pages_users WHERE user_id = ?) OR namespace_id IN (SELECT id FROM namespaces WHERE access = 'everyone' OR access = 'members')", user.id] do |page|
        page.namespace.access == 'everyone' || page.namespace.access == 'members' || page.users.include?(user)
      end

      can [:read, :create], Vote
      can [:update, :destroy], Vote, user_id: user.id
      can :vote, Page

      cannot :create, User
      cannot :create, :session
      cannot :create, :password_reset
      can :read, User
      can :update, User, id: user.id
      can :destroy, :session

      if user.staffer?
        can [:read, :use], Namespace, access: 'staffers'
        can :use, Namespace, access: 'everyone'
        can [:read, :comment, :update], Page, ["namespace_id IN (SELECT id FROM namespaces WHERE access = 'staffers')"] do |page|
          page.namespace.access == 'staffers'
        end
        can :manage, Warning
        can :use, :admin_namespace
        
        if user.admin?
          can [:read, :comment, :update, :destroy], Page, ["id IN (SELECT page_id FROM pages_users WHERE user_id = ?) OR namespace_id IN (SELECT id FROM namespaces WHERE access = 'everyone' OR access = 'members' OR access = 'staff' OR access = 'admins')", user.id] do |page|
            page.namespace.access == 'everyone' ||
            page.namespace.access == 'members'  ||
            page.namespace.access == 'staff'    ||
            page.namespace.access == 'admins'   ||
            page.users.include?(user)
          end
          can [:read, :use], Namespace
          can [:read, :create, :destroy], Invite
          can :manage, Tag
          can :destroy, Vote
          can :destroy, Comment
          can :update, User
        end
      end
    end
  end
end
