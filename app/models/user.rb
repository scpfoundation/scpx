class User < ActiveRecord::Base
  has_secure_password validations: false

  attr_accessor :invite_code

  has_and_belongs_to_many :pages
  has_many :notifications
  has_many :warnings

  before_create :prepare_values, :generate_auth_token

  validates :real_name, format: /\A[\wа-яА-Я\d\s\-]*\Z/, if: :real_name_changed?
  validates :username, uniqueness: { case_sensitive: false }, format: /\A[\w\_\-\d]+\Z/,  length: { within: 2..30 }
  validates :email, uniqueness: { case_sensitive: false }, email: true
  validates :password, confirmation: true, length: { minimum: 6 }, if: :validate_password?

  def to_param
    username
  end

  def invited_by
    @invited_by ||= User.find Invite.find_by_email(email).user_id
  end

  def send_password_reset
    generate_token :password_reset_token
    self.password_reset_sent_at = Time.zone.now
    save!
    UserMailer.password_reset(self).deliver
  end

  def generate_auth_token
    generate_token :auth_token
  end

  private

  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end

  def prepare_values
    write_attribute :display_name, read_attribute(:username)
    write_attribute :username, read_attribute(:username).downcase.gsub(' ', '_')
  end

  def validate_password?
    password.present? || password_confirmation.present?
  end
end
