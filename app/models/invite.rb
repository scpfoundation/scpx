class Invite < ActiveRecord::Base
  before_create :generate_token, :check_email
  validates :email, uniqueness: { case_sensitive: false }, email: true

  belongs_to :user

  def invited_user
    @invited_user ||= User.find_by_email(email)
  end

  private

  def generate_token
    begin
      self[:token] = SecureRandom.urlsafe_base64
    end while Invite.exists?(token: self[:token])
  end

  def check_email
    if User.find_by_email(email) || Invite.find_by_email(email)
      errors[:email] << 'User or invite with the same email exists'
      false
    end
  end
end
