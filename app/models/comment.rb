class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :page

  has_ancestry

  # validates_associated :page, :user
  validates_presence_of :content

  before_destroy :destroy_children

  private

  def destroy_children
    if children
      children.each do |child|
        child.destroy
      end
    end
  end
end
