class Warning < ActiveRecord::Base
  belongs_to :user

  def staffer
    @staffer ||= User.find staffer_id
  end
end
