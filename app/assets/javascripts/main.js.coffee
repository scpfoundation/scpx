$ ->
  unless typeof operamini is "undefined" # Check to see if operamini a JS var added by Opera Mini browser exists so other browsers won't error
    if operamini # This should only work for Opera Mini
      $("#menuButton").click (e) -> # Bind click which Opera Mini likes better
        e.preventDefault() # Prevent default action
        # Manually add collapse to the targeted button
        $(this).collapse toggle: true

  # $(".fold_action").bind 'click', ->
  $(document).on 'click', '.fold_action', ->
    parent = $(this).parent()
    parent.find(".spoiler").toggle "fast"
    if $(this).html() == parent.find(".show_action").html()
      $(this).html parent.find(".hide_action").html()
    else
      $(this).html parent.find(".show_action").html()

  $('a[rel=external]').attr('target','_blank')

  flash_message = (content) ->
    $('div#flash').html("<div class='alert alert-success' id='new_flash'>#{content}</div>")
      .fadeOut().fadeIn().fadeOut().fadeIn()
      .click ->
        $('div#flash').html ''


  load_editor = ->
    content_textarea = $('textarea.wmd-input')
    if content_textarea.length != 0
      $('.wmd-input').before('<div id="wmd-button-bar"></div>')
      converter = new Markdown.Converter()
      help = -> (alert 'NO HELP FOR YOU')
      options =
        helpButton: { handler: help }
        strings: { quioteexample: 'Hello, World!' }
      editor = new Markdown.Editor(converter, '', options)
      editor.run()

  toggle_sidebar = ->
    if $(window).width() < 768
      # $('#content-block').width ($(window).width() - 80)
      $('#sidebar').toggle 'hide'
    else
      if $('#sidebar').is(':visible')
        $('#content-block').width ($(window).width() - 80)
      else
        $('#content-block').width ($(window).width() - $('#sidebar').width() - 80)
      $('#sidebar').toggle 'hide'

  resize_logs = ->
    messagesDiv = document.getElementById 'messages_container'
    if messagesDiv != null
      toggle_sidebar() if $('#sidebar').is(':visible')
      bodyheight = $(window).height()
      if $(window).width() < 768
        $("#messages_container").height(bodyheight-20)
      else
        $("#messages_container").height(bodyheight-240)
      messagesDiv.scrollTop = messagesDiv.scrollHeight


  $(document).ready ->
    load_editor()
    resize_logs()
    Hyphenator.run()

  $(window).resize ->
    resize_logs()

  $(window).bind('page:change', load_editor)
  $(window).bind('page:change', Hyphenator.run)
  $(window).bind('page:change', resize_logs)

  $(document).on 'click', '.toggle_sidebar', ->
    toggle_sidebar()
    false

  window.resize_logs = resize_logs
