$(document).on 'click', "#pages_preview_button", ->
  action = $(this).attr("href")
  content = $.param { content: $("#wmd-input").val() }
  title = $("#page_title").val()
  $.ajax
    type: "POST"
    url: action
    data: content
    async: true
    success: (data) ->
      $("#page_preview_title").html title
      $("#page_preview_content").html data

    error: ->
      alert "Something went wrong."

    complete: ->
      false

  false
