resize_logs = ->
  messagesDiv = document.getElementById 'messages_container'
  if messagesDiv != null
    bodyheight = $(window).height()
    $("#messages_container").height(bodyheight-240)
    messagesDiv.scrollTop = messagesDiv.scrollHeight

$(document).ready ->
  resize_logs()
$(window).resize ->
  resize_logs()
$(window).bind('page:change', resize_logs)
