$(document).on 'click', "#comments_preview_button", ->
  action = $(this).attr("href")
  content = $.param { content: $("#wmd-input").val() }
  $.ajax
    type: "POST"
    url: action
    data: content
    async: true
    success: (data) ->
      $("#comment_preview_content").html data

    error: ->
      alert "Something went wrong."

    complete: ->
      false

  false

$(document).on 'click', '.reply-link', ->
  $("#reply_form").appendTo $(this).parent().parent()
  $("#reply_form").show()
  parent_id =  $(this).parent().parent()[0].id.slice(3)

  comment_parent_id = $('#comment_parent_id')
  comment_parent_id.remove() unless comment_parent_id == []

  unless parent_id == ""
    $('<input>').attr({
      type: 'hidden',
      id: 'comment_parent_id',
      name: 'comment[parent_id]',
      value: parent_id
    }).appendTo('#new_comment')

  false

$(document).on 'click', "#comments_cancel_reply_button", ->
  $("#reply_form").hide()

  false
