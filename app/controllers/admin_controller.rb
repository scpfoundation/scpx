class AdminController < ApplicationController
  before_action :check_access

  def warnings
    users = User.joins(:warnings)
    count = users.count(group: 'users.id')
    @warnings = []

    count.each do |c|
      @warnings.push [users.select { |u| u.id == c[0] }.first, c[1]]
    end

    @warnings = @warnings.sort_by { |w| w[1] }.reverse.paginate(per_page: 30, page: params[:warning])
  end

  private

  def check_access
    can? :use, :admin_namespace
  end
end
