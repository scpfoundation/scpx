class MessagesController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: :create

  def index
    @messages = Message.where(channel: '#scp').paginate(page: params[:page], per_page: 100, order: "created_at DESC")
    authorize! :read, Message
  end

  def show
    @message = Message.find params[:id]
    authorize! :read, Message
  end

  def create
    access_token = AccessToken.where(name: params[:token_name]).first

    if access_token.nil? or access_token.token != params[:token_value]
      render json: { response: 'access denied' }
    else
      @message = Message.new message_params
      if @message.save
        PrivatePub.publish_to "/messages/scp/new",
          render_to_string('messages/update_messages.js.erb', layout: false)
        render json: { response: @message.id }
      else
        render json: { response: 'save error' }
      end
    end
  end

  private
  
  def message_params
    params.require(:message).permit(:nickname, :authname, :content, :channel, :message_type)
  end
end
