class PasswordResetsController < ApplicationController
  def new
    authorize! :create, :password_reset
  end

  def create
    authorize! :create, :password_reset
    user = User.find_by_username(params[:user][:username])
    if user
      user.send_password_reset
      redirect_to :root, notice: t('password_resets.email_send_notice')
    else
      redirect_to new_password_reset_path, alert: t('password_resets.no_user_warning')
    end
  end

  def edit
    authorize! :create, :password_reset
    @user = User.find_by_password_reset_token! params[:id]
  end

  def update
    authorize! :create, :password_reset
    @user = User.find_by_password_reset_token! params[:id]
    if @user.password_reset_sent_at < 2.hours.ago
      redirect_to new_password_reset_path, alert: t('password_resets.expired_reset_alert')
    elsif @user.update_attributes(user_params)
      redirect_to root_url, notice: t('password_resets.password_reset_notice')
    else
      render :edit
    end
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end
end
