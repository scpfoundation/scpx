class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :load_namespace

  include UrlHelper

  rescue_from CanCan::AccessDenied do |exception|
    # render 'pages/access_denied', status: 403
    if current_user
      redirect_to root_url(subdomain: nil), alert: 'Access denied'
    else
      redirect_to login_url(subdomain: nil), alert: 'Access denied'
    end
  end

  private

  def load_namespace
    if request.subdomain.blank?
      @namespace ||= Namespace.find_by_name 'main'
    else
      @namespace ||= Namespace.find_by_name request.subdomain
    end
    authorize! :read, @namespace
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  helper_method :current_user
end
