class VotesController < ApplicationController
  before_filter :authorize_resources

  # GET /votes
  # GET /votes.json
  def index
    @votes = @page.votes
    @vote = @votes.where(user_id: current_user.id).first

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @votes }
    end
  end

  # GET /votes/1
  # GET /votes/1.json
  # def show
  #   @vote = @page.votes.find(params[:id])

  #   respond_to do |format|
  #     format.html # show.html.erb
  #     format.json { render json: @vote }
  #   end
  # end

  # GET /votes/new
  # GET /votes/new.json
  # def new
  #   @vote = @page.votes.build

  #   respond_to do |format|
  #     format.html # new.html.erb
  #     format.json { render json: @vote }
  #   end
  # end

  # GET /votes/1/edit
  # def edit
  #   @vote = @page.votes.find(params[:id])
  # end

  # POST /votes
  # POST /votes.json
  def create
    @vote = @page.votes.build(vote_params)
    @vote.user = current_user

    if @page.votes.where(user_id: current_user.id).count > 0
      redirect_to page_votes_path(@page), alert: t('.duplicate_vote_alert')
    elsif @vote.save
      redirect_to page_votes_path(@page), notice: t('.vote_created_notice')
    else
      redirect_to page_votes_path(@page), alert: t('.vote_wasnt_created_alert')
    end
  end

  # PUT /votes/1
  # PUT /votes/1.json
  def update
    @vote = @page.votes.find(params[:id])

    if @vote.update_attributes(vote_params)
      redirect_to page_votes_path(@page), notice: t('.vote_updated_notice')
    else
      redirect_to page_votes_path(@page), alert: t('.vote_wasnt_created_alert')
    end
  end

  # DELETE /votes/1
  # DELETE /votes/1.json
  def destroy
    @vote = @page.votes.find(params[:id])
    @vote.destroy

    respond_to do |format|
      format.html { redirect_to page_votes_url(@page), notice: t('.vote_destroyed_notice') }
      format.json { head :no_content }
    end
  end

  private

  def vote_params
    params.require(:vote).permit(:value, :staff, :admin)
  end

  def authorize_resources
    @page = @namespace.pages.find_by_name(params[:page_id])
    authorize! :vote, @page
    authorize! :read, @page
  end
end

