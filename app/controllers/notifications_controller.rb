class NotificationsController < ApplicationController
  load_and_authorize_resource
  
  # GET /notifications
  # GET /notifications.json
  def index
    @notifications = current_user.notifications.paginate(per_page: 30, page: params[:page]).order('created_at DESC')
  end

  # GET /notifications/1
  # GET /notifications/1.json
  def show
    @notification.update_attribute :read, true
    update_notifications
    if @notification.url
      redirect_to @notification.url
    else
      redirect_to notifications_url, notice: 'Notification was marked as read'
    end
  end

  # DELETE /notifications/1
  # DELETE /notifications/1.json
  def destroy
    @notification.destroy

    update_notifications
    redirect_to notifications_url
  end
  
  def read_all
    @notifications = current_user.notifications
    @notifications.each do |n|
      n.update_attribute(:read, true)
    end
    update_notifications
    redirect_to notifications_path, notice: 'All notifications were marked as read'
  end
  
  def destroy_read
    @notifications = Notification.where(user_id: current_user.id, read: true)
    @notifications.each { |n| n.destroy }
    update_notifications
    redirect_to notifications_path, notice: 'All read notifications were deleted'
  end

  private

  def update_notifications
    @username = current_user.username
    @counter = Notification.where(user_id: current_user.id, read: false).count

    PrivatePub.publish_to "/notifications/#{@username}",
      render_to_string('notifications/update_notifications.js.erb', layout: false)
  end
end

