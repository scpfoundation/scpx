class CommentsController < ApplicationController
  before_filter :authorize_resources
  authorize_resource through: :page
  before_filter :check_comment_ability, only: [:new, :create, :edit, :update]

  def index
    @comments = @page.comments
  end

  def show
    @comment = @page.comments.find(params[:id])
  end

  def new
    @comment = @page.comments.build
  end

  def create
    parent_id = comment_params[:parent_id] || nil
    parent_id = nil if parent_id && Comment.find(comment_params[:parent_id]).page != @page
    @comment = @page.comments.build comment_params.merge parent_id: parent_id
    @comment.user = current_user

    if @comment.save
      PrivatePub.publish_to "/pages/#{@page.id}/comments",
        render_to_string('comments/update_comments.js.erb', layout: false)


      # User notifications
      @comment.content.scan(/@[\w\_\-\d]+/).uniq.each do |username|
        user = User.find_by_username(username[1..-1].downcase)
        unless user.nil?
          @username = user.username
          
          notification_url = root_url(subdomain: @namespace.name) + "#{@page.name}"
          notification_url += "/comments" unless @namespace.discussion
          notification_url += "#msg#{@comment.id}"

          notification =  { content: "You were mentioned at the discussion \"#{@comment.page.title}\"",
                          url: notification_url }

          user.notifications.build(notification).save
          @counter = user.notifications.where(read: false).count

          PrivatePub.publish_to "/notifications/#{@username}",
            render_to_string('notifications/update_notifications.js.erb', layout: false)
        end
      end
      
      redirect_to discussion_path, notice: 'Reply was added'
    else
      redirect_to discussion_path, alert: @comment.errors.first
    end
  end

  def edit
    @comment = @page.comments.find(params[:id])
  end

  def update
    @comment = @page.comments.find(params[:id])

    if @comment.update_attributes(comment_params)
      redirect_to discussion_path, notice: 'Comment was updated.'
    else
      render action: 'edit', alert: @comment.errors.first
    end
  end

  def destroy
    @comment = @page.comments.find params[:id]

    if @comment.destroy
      redirect_to discussion_path, notice: 'Comment deleted'
    else
      render action: 'edit'
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :parent_id)
  end

  def discussion_path
    if @namespace.discussion?
      page_path @page
    else
      page_comments_path @page
    end
  end

  def authorize_resources
    @page = @namespace.pages.find_by_name(params[:page_id])
    authorize! :read, @page
  end

  def check_comment_ability
    authorize! :comment, @page
  end
end
