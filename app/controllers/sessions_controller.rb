class SessionsController < ApplicationController
  def new
    authorize! :create, :session
  end

  def create
    authorize! :create, :session

    user = User.find_by_username(session_params[:username].downcase)
    if user && user.authenticate(session_params[:password])
      # if params[:remember_me]
      #   cookies.permanent.signed[:auth_token] = { value: user.auth_token, domain: :all }
      # else
      #   cookies.signed[:auth_token] = { value: user.auth_token, domain: :all }
      # end
      session[:user_id] = user.id
      redirect_to root_url, notice: t('.logged_in_notice')
    else
      flash.now.alert = t('.invalid_password_alert')
      render 'new'
    end
  end

  def destroy
    authorize! :destroy, :session
    # cookies.signed[:auth_token] = { value: '', domain: :all }
    session[:user_id] = nil
    redirect_to root_url(subdomain: ''), notice: t('.logged_out_notice')
  end

  private

  def session_params
    params.require(:session).permit(:username, :password)
  end
end
