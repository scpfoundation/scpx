class UsersController < ApplicationController
  authorize_resource

  def new
    @user = User.new
  end
  
  def create
    @user = User.new user_params
    invite = Invite.find_by_token user_params[:invite_code]
    @user.email = invite.email if invite

    if invite.nil?
      flash.alert = 'Wrong invite code'
      render 'new'
    elsif @user.save
      # cookies.signed[:auth_token] = { value: @user.auth_token, domain: :all }
      session[:user_id] = @user.id
      redirect_to root_url, notice: t('.signed_up')
    else
      render 'new'
    end
  end

  def show
    @user = User.find_by_username params[:id].downcase
  end

  def edit
    @user = User.find_by_username params[:id].downcase
  end

  def update
    @user = User.find_by_username params[:id].downcase

    if @user.update(user_params)
      redirect_to @user, notice: 'Profile was successfully updated.'
    else
      render action: :edit
    end
  end

  private

  def user_params
    params.require(:user).permit(:username, :password, :password_confirmation, :real_name, :email, :about, :invite_code)
  end

  def update_user_params
    params.require(:user).permit(:password, :password_confirmation, :real_name, :about)
  end
end
