class WarningsController < ApplicationController
  before_action :load_user
  before_action :set_warning, only: [:show, :edit, :update, :destroy]
  authorize_resource

  # GET /warnings
  # GET /warnings.json
  def index
    @warnings = @user.warnings.all
  end

  # GET /warnings/1
  # GET /warnings/1.json
  def show
  end

  # GET /warnings/new
  def new
    @warning = @user.warnings.build
  end

  # GET /warnings/1/edit
  def edit
  end

  # POST /warnings
  # POST /warnings.json
  def create
    @warning = @user.warnings.build(warning_params.merge(staffer_id: current_user.id, user_id: @user.id))

    respond_to do |format|
      if @warning.save
        format.html { redirect_to [@user, @warning], notice: 'Warning was successfully created.' }
        format.json { render action: 'show', status: :created, location: @warning }
      else
        format.html { render action: 'new' }
        format.json { render json: @warning.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /warnings/1
  # PATCH/PUT /warnings/1.json
  def update
    respond_to do |format|
      if @warning.update(warning_params)
        format.html { redirect_to [@user, @warning], notice: 'Warning was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @warning.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /warnings/1
  # DELETE /warnings/1.json
  def destroy
    @warning.destroy
    respond_to do |format|
      format.html { redirect_to warnings_url }
      format.json { head :no_content }
    end
  end

  private
    def load_user
      @user = User.find_by_username params[:user_id]
    end

    def set_warning
      @warning = @user.warnings.find(params[:id])
    end

    def warning_params
      params.require(:warning).permit(:description)
    end
end
