json.array!(@warnings) do |warning|
  json.extract! warning, :user_id, :description, :staffer_id
  json.url warning_url(warning, format: :json)
end