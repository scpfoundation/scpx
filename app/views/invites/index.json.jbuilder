json.array!(@invites) do |invite|
  json.extract! invite, :email, :user_id, :invited_user_id, :token
  json.url invite_url(invite, format: :json)
end